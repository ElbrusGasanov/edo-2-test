Для разворачивания проекта:
- Установить Docker и Docker Compose
- Клонировать репозиторий
- В корневой директории вызвать docker-compose build
- Там же вызвать docker-compose up