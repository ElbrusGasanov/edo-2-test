import os

from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from programs.api.urls import (
    program_router,
    declaration_router,
)

# основные эндпоинты, доступные всем регионам по умолчанию,
# прописывать тут
default_routers = {
    "program": program_router,
    "declaration": declaration_router,
    # тут можно разместить другие роутеры
}

# тут прописывать эндпоинты, которые недоступны для регионов.
# ключ - это конкретный ключ из словаря выше.
exclude_ruoters = {
    "tyumen" : {},
    "rostov": {},
    "murmansk": {
        "declaration",
    },
}

region = os.getenv("REGION")

# фильтрация роутов согласно exclude_routers
filtered_urls = [
    path("api/", include(router.urls))
    for key, router
    in default_routers.items()
    if (
        region in exclude_ruoters
        and key not in exclude_ruoters[region]
    )
]


urlpatterns = [
    path('admin/', admin.site.urls),
    *filtered_urls,
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "api/schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path(
        "api/schema/redoc/",
        SpectacularRedocView.as_view(url_name="schema"),
        name="redoc",
    ),
    path("__debug__/", include("debug_toolbar.urls")),
]
