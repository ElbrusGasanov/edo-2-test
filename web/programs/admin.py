from django.contrib import admin
from programs.models import (
    EducationalProgram,
    Declaration,
    Child,
    Parent,
)


admin.site.register(EducationalProgram)
admin.site.register(Declaration)
admin.site.register(Child)
admin.site.register(Parent)