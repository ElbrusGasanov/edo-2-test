from rest_framework import serializers
from programs.models import Declaration, EducationalProgram, Child, Parent


class ProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = EducationalProgram
        fields = [
            "name",
            "description",
            "max_age",
            "min_age",
            "owner_organization",
            "cost",
        ]


class ProgramTyumenSerializer(ProgramSerializer):
    class Meta(ProgramSerializer.Meta):
        fields = ProgramSerializer.Meta.fields + [
            "discount_for_disabled",  # региональное поле
        ]


class ProgramRostovSerializer(serializers.ModelSerializer):
    class Meta(ProgramSerializer.Meta):
        fields = ProgramSerializer.Meta.fields + [
            "discount_for_deputies_children",  # региональное поле
            "for_deef_and_mute_children",  # региональное поле
        ]


class DeclarationSerializer(serializers.ModelSerializer):
    program = serializers.PrimaryKeyRelatedField(
        queryset=EducationalProgram.objects.all(),
    )
    child = serializers.PrimaryKeyRelatedField(
        queryset=Child.objects.all(),
    )
    declarant = serializers.PrimaryKeyRelatedField(
        queryset=Parent.objects.all(),
    )

    class Meta:
        model = Declaration
        fields = [
            "program",
            "child",
            "declarant",
        ]