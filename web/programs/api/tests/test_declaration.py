import os
from rest_framework.test import APITestCase, override_settings
from programs.models import Child, EducationalProgram, Parent


class DeclarationAPITestCase(APITestCase):
    def setUp(self):
        self.child = Child.objects.create(
            first_name="Ivan", 
            last_name="Ivanov",
            patronymic="Sergeyevich",
            is_deputis_children=True,
        )
        self.parent = Parent.objects.create(
            first_name="Sergey", 
            last_name="Ivanov",
            patronymic="Ivanovich",
        )
        self.program = EducationalProgram.objects.create(
            name="Test program",
            description="Description",
            max_age=15,
            min_age=12,
            owner_organization="ООО Рога и Копыта",
            cost=10000,
            # ====== for Tyumen ======
            discount_for_disabled=15,
            # ====== for Rostov ======
            discount_for_deputies_children=5,
        )

        self.url = '/api/declaration/'
    
    def tearDown(self):
        del os.environ['REGION']
        super().tearDown()


class DeclarationForTymenAPITestCase(DeclarationAPITestCase):
    def setUp(self):
        print()
        super().setUp()
        os.environ['REGION'] = 'tyumen'

    def test_post_request(self):
        print("=====Запущен тест для Тюмени=======")
        response = self.client.post(
            self.url,
            data=dict(
                program=self.program.pk,
                child=self.child.pk,
                declarant=self.parent.pk,
            ),
            format="json",
        )
        self.assertEqual(response.status_code, 201)


class DeclarationForRostovAPITestCase(DeclarationAPITestCase):
    def setUp(self):
        super().setUp()
        os.environ['REGION'] = 'rostov'
    
    def test_post_request(self):
        print("=====Запущен тест для Ростова=======")
        response = self.client.post(
            self.url,
            data=dict(
                program=self.program.pk,
                child=self.child.pk,
                declarant=self.parent.pk,
            ),
            format="json",
        )
        self.assertEqual(response.status_code, 201)