from rest_framework.routers import SimpleRouter

from programs.api.views import ProgramViewSet, DeclarationViewSet


program_router = SimpleRouter()
program_router.register(r"program", ProgramViewSet, basename="program")

declaration_router = SimpleRouter()
declaration_router.register(r"declaration", DeclarationViewSet, basename="declaration")

# тут можно для других апи в рамках данной аппы добавить другие роутеры