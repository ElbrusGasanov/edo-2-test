import os

from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import (
    ListModelMixin,
    CreateModelMixin,
)
from programs.api.serializers import (
    DeclarationSerializer,
    ProgramSerializer,
    ProgramRostovSerializer,
    ProgramTyumenSerializer,
)
from programs.models import Declaration, EducationalProgram
from programs.api.regions.tyumen import tyumen_create_declration


class ProgramViewSet(ListModelMixin, GenericViewSet):
    queryset = EducationalProgram.objects.all()

    def get_serializer_class(self):
        region = os.getenv("REGION")
        if region == 'tyumen':
            return ProgramTyumenSerializer
        elif region == 'rostov':
            return ProgramRostovSerializer
        else:
            return ProgramSerializer


class DeclarationViewSet(
    CreateModelMixin,
    GenericViewSet,
):

    queryset = Declaration.objects.all()
    serializer_class = DeclarationSerializer

    def create(self, request, *args, **kwargs):
        region = os.getenv("REGION")
        print(f"region: {region}")
        if region == 'tyumen':
            return tyumen_create_declration(
                DeclarationViewSet,
                self,
                request,
                *args,
                **kwargs,
            )
        print("Выполнилась дефолтная логика")
        return super().create(request, *args, **kwargs)
