from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 


class Parent(models.Model):
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    patronymic = models.CharField(max_length=64)    


class Child(models.Model):
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    patronymic = models.CharField(max_length=64)

    is_disabled = models.BooleanField(default=False)
    is_deaf_and_mute = models.BooleanField(default=False)
    is_deputis_children = models.BooleanField(default=False)


class EducationalProgram(models.Model):
    # common fields
    name = models.CharField(max_length=128)
    description = models.TextField()
    max_age = models.PositiveSmallIntegerField()
    min_age = models.PositiveSmallIntegerField()
    owner_organization = models.CharField(max_length=128)

    cost = models.DecimalField(decimal_places=2, max_digits=8)

    # for Tyumen
    discount_for_disabled = models.PositiveSmallIntegerField(
        default=0,
        validators=[
            MinValueValidator(0), 
            MaxValueValidator(100),
        ],
        verbose_name="скидка для детей-инвалидов, %",
    )

    # for Rostov
    discount_for_deputies_children = models.PositiveSmallIntegerField(
        default=0,
        validators=[
            MinValueValidator(0), 
            MaxValueValidator(100),
        ],
        verbose_name="скидка для детей депутатов, %",
    )
    for_deef_and_mute_children = models.BooleanField(default=False)


class Declaration(models.Model):
    child = models.ForeignKey(
        to=Child,
        on_delete=models.CASCADE,
        related_name='declataions',
    )
    declarant = models.ForeignKey(
        to=Parent,
        on_delete=models.CASCADE,
        related_name='declataions',
    )
    program = models.ForeignKey(
        to=EducationalProgram,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )